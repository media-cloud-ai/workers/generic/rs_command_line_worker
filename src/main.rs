#[macro_use]
extern crate serde_derive;

use mcai_worker_sdk::prelude::{
  info, start_worker, JobResult, JsonSchema, McaiChannel, McaiWorker, MessageError, Version,
};
use std::collections::HashMap;

mod message;

pub mod built_info {
  include!(concat!(env!("OUT_DIR"), "/built.rs"));
}

#[derive(Debug, Default)]
struct CommandLineEvent {}

#[derive(Debug, Deserialize, JsonSchema)]
pub struct CommandLineWorkerParameters {
  command_template: String,
  exec_dir: Option<String>,
  #[serde(flatten)]
  parameters: HashMap<String, String>,
  #[allow(dead_code)]
  requirements: Option<HashMap<String, Vec<String>>>,
  #[serde(default)]
  #[allow(dead_code)]
  source_paths: Vec<String>,
  #[serde(default)]
  #[allow(dead_code)]
  destination_paths: Vec<String>,
}

impl McaiWorker<CommandLineWorkerParameters> for CommandLineEvent {
  fn get_name(&self) -> String {
    "Command Line".to_string()
  }

  fn get_short_description(&self) -> String {
    "Execute command lines on workers".to_string()
  }

  fn get_description(&self) -> String {
    r#"Run any command line available in the command line of the worker.
Provide a template parameter, other parameters will be replaced before running."#
      .to_string()
  }

  fn get_version(&self) -> Version {
    Version::parse(built_info::PKG_VERSION).expect("unable to locate Package version")
  }

  fn process(
    &self,
    channel: Option<McaiChannel>,
    parameters: CommandLineWorkerParameters,
    job_result: JobResult,
  ) -> Result<JobResult, MessageError> {
    let job_id = &job_result.get_str_job_id();
    info!(target: job_id, "START_JOB");
    let job_result = message::process(channel, parameters, job_result);
    info!(target: job_id, "END_JOB");
    job_result
  }
}

fn main() {
  let message_event = CommandLineEvent::default();
  start_worker(message_event);
}
