use crate::{CommandLineEvent, CommandLineWorkerParameters};
use mcai_worker_sdk::prelude::{
  warn, JobResult, JobStatus, McaiChannel, McaiWorker, MessageError, Result,
};
use std::{
  collections::HashMap,
  io::Read,
  path::Path,
  process::{Child, Command, Stdio},
  time::Duration,
};

const COMMAND_TEMPLATE_IDENTIFIER: &str = "command_template";
const EXECUTION_DIRECTORY_PARAMETER: &str = "exec_dir";

const INTERNAL_PARAM_IDENTIFIERS: [&str; 2] =
  [COMMAND_TEMPLATE_IDENTIFIER, EXECUTION_DIRECTORY_PARAMETER];

pub fn process(
  channel: Option<McaiChannel>,
  parameters: CommandLineWorkerParameters,
  job_result: JobResult,
) -> Result<JobResult> {
  let command = compile_command_template(parameters.command_template, parameters.parameters);
  if command.is_empty() {
    warn!(target: &job_result.get_str_job_id(), "The command line is empty, nothing to execute.");
    return Ok(job_result.with_status(JobStatus::Completed));
  }
  let mut child_process = launch(command, parameters.exec_dir).map_err(|msg| {
    MessageError::ProcessingError(
      job_result
        .clone()
        .with_status(JobStatus::Error)
        .with_message(&msg),
    )
  })?;

  let pid = child_process.id();

  loop {
    if CommandLineEvent::is_current_job_stopped(&channel) {
      child_process.kill().map_err(|error| {
        MessageError::RuntimeError(format!("Cannot kill running process {}: {}", pid, error))
      })?;
      let (stdout_content, _stderr_content) = get_process_output(child_process)?;
      return Ok(
        job_result
          .with_status(JobStatus::Stopped)
          .with_message(&stdout_content),
      );
    }

    match child_process.try_wait() {
      Ok(Some(status)) => {
        let (stdout_content, stderr_content) = get_process_output(child_process)?;

        return if status.success() {
          Ok(
            job_result
              .with_status(JobStatus::Completed)
              .with_message(&stdout_content),
          )
        } else {
          let mut error_message = format!("{}\n{}", stderr_content, stdout_content);
          error_message.truncate(1024 * 1024);
          Err(MessageError::ProcessingError(
            job_result
              .with_status(JobStatus::Error)
              .with_message(&error_message),
          ))
        };
      }
      Ok(None) => {
        std::thread::sleep(Duration::from_millis(10));
      }
      Err(error) => {
        return Err(MessageError::ProcessingError(
          job_result
            .with_status(JobStatus::Error)
            .with_message(&error.to_string()),
        ))
      }
    }
  }
}

fn compile_command_template(
  command_template: String,
  param_map: HashMap<String, String>,
) -> String {
  let mut compiled_command_template = command_template;
  param_map
    .iter()
    .filter(|(key, _value)| !INTERNAL_PARAM_IDENTIFIERS.contains(&key.as_str()))
    .for_each(|(key, value)| {
      compiled_command_template = compiled_command_template.replace(&format!("{{{}}}", key), value)
    });
  compiled_command_template
}

fn launch(command: String, exec_dir: Option<String>) -> std::result::Result<Child, String> {
  let mut process = Command::new("bash");
  if let Some(current_dir) = exec_dir {
    process.current_dir(Path::new(&current_dir));
  }

  let child_process = process
    .arg("-c")
    .arg(&command)
    .stdout(Stdio::piped())
    .stderr(Stdio::piped())
    .spawn()
    .map_err(|error| {
      format!(
        "An error occurred process command: {}.\n{:?}",
        command, error
      )
    })?;
  Ok(child_process)
}

fn get_process_output(child_process: Child) -> Result<(String, String)> {
  let mut stdout_content = String::new();
  let mut stderr_content = String::new();
  let pid = child_process.id();

  match (child_process.stdout, child_process.stderr) {
    (Some(mut stdout), Some(mut stderr)) => {
      stdout
        .read_to_string(&mut stdout_content)
        .map_err(|error| {
          MessageError::RuntimeError(format!(
            "Cannot read process {} standard output, {}",
            pid, error
          ))
        })?;
      stderr
        .read_to_string(&mut stderr_content)
        .map_err(|error| {
          MessageError::RuntimeError(format!(
            "Cannot read process {} standard output, {}",
            pid, error
          ))
        })?;
    }
    (Some(mut stdout), None) => {
      stdout
        .read_to_string(&mut stdout_content)
        .map_err(|error| {
          MessageError::RuntimeError(format!(
            "Cannot read process {} standard output, {}",
            pid, error
          ))
        })?;
    }
    (None, Some(mut stderr)) => {
      stderr
        .read_to_string(&mut stderr_content)
        .map_err(|error| {
          MessageError::RuntimeError(format!(
            "Cannot read process {} standard output, {}",
            pid, error
          ))
        })?;
    }
    (None, None) => {}
  }

  Ok((stdout_content, stderr_content))
}

#[test]
pub fn test_compile_command_template() {
  let command_template = "ls {option} {path}".to_string();
  let mut parameters = HashMap::new();
  parameters.insert("option".to_string(), "-l".to_string());
  parameters.insert("path".to_string(), ".".to_string());

  let command = compile_command_template(command_template, parameters);
  assert_eq!("ls -l .", command.as_str());
}

#[test]
pub fn test_compile_command_template_with_doubles() {
  let command_template = "ls {option} {path} {option}".to_string();
  let mut parameters = HashMap::new();
  parameters.insert("option".to_string(), "-l".to_string());
  parameters.insert("path".to_string(), ".".to_string());

  let command = compile_command_template(command_template, parameters);
  assert_eq!("ls -l . -l", command.as_str());
}

#[test]
pub fn test_compile_command_template_with_fixed_params() {
  let command_template = "ls {option} {path}".to_string();
  let mut parameters = HashMap::new();
  parameters.insert("option".to_string(), "-l".to_string());
  parameters.insert("path".to_string(), ".".to_string());
  parameters.insert(
    COMMAND_TEMPLATE_IDENTIFIER.to_string(),
    command_template.clone(),
  );
  parameters.insert(
    EXECUTION_DIRECTORY_PARAMETER.to_string(),
    "/path/to/somewhere".to_string(),
  );

  let command = compile_command_template(command_template, parameters);
  assert_eq!("ls -l .", command.as_str());
}

#[test]
pub fn test_launch() {
  let command = "ls .".to_string();
  let exec_dir = None;
  let result = launch(command, exec_dir);
  assert!(result.is_ok());

  let process = result.unwrap();
  let (std_out, std_error) = get_process_output(process).unwrap();
  assert!(std_out.contains("Cargo.toml"));
  assert!(std_out.contains("Cargo.lock"));
  assert!(std_error.is_empty());
}

#[test]
pub fn test_launch_with_exec_dir() {
  let command = "ls .".to_string();
  let exec_dir = Some("./src".to_string());
  let result = launch(command, exec_dir);
  assert!(result.is_ok());

  let process = result.unwrap();
  let (std_out, std_error) = get_process_output(process).unwrap();
  assert!(std_out.contains("main.rs"));
  assert!(std_out.contains("message.rs"));
  assert!(std_error.is_empty());
}

#[test]
pub fn test_launch_error() {
  let command = "ls sdjqenfdcnekbnbsdvjhqr".to_string();
  let exec_dir = None;
  let result = launch(command, exec_dir);
  assert!(result.is_ok());

  let process = result.unwrap();
  let (std_out, std_error) = get_process_output(process).unwrap();
  assert!(std_out.is_empty());
  assert!(std_error.contains("ls:"));
  assert!(std_error.contains("sdjqenfdcnekbnbsdvjhqr"));
}

#[test]
pub fn test_process_with_empty_command() {
  use mcai_worker_sdk::job::Job;

  let message = r#"{
    "job_id": 123,
    "parameters": [
      {
        "id": "command_template",
        "type": "string",
        "value": ""
      }
      ]
  }"#;
  let job = Job::new(message).unwrap();
  let job_result = JobResult::new(job.job_id);
  let parameters: CommandLineWorkerParameters = job.get_parameters().unwrap();
  let result = process(None, parameters, job_result);
  assert!(result.is_ok());
}

#[test]
pub fn test_process() {
  use mcai_worker_sdk::job::Job;
  use mcai_worker_sdk::ParametersContainer;

  let message = r#"{
    "job_id": 123,
    "parameters": [
      {
        "id": "command_template",
        "type": "string",
        "value": "ls {option} {path}"
      },
      {
        "id": "option",
        "type": "string",
        "value": "-lh"
      },
      {
        "id": "path",
        "type": "string",
        "value": "."
      },
      {
        "id": "exec_dir",
        "type": "string",
        "value": "./src"
      }
    ]
  }"#;

  let job = Job::new(message).unwrap();
  let job_result = JobResult::new(job.job_id);
  let parameters: CommandLineWorkerParameters = job.get_parameters().unwrap();
  let result = process(None, parameters, job_result);

  assert!(result.is_ok());
  let job_result = result.unwrap();
  assert_eq!(123, job_result.get_job_id());
  assert_eq!(&JobStatus::Completed, job_result.get_status());
  let message_param = job_result.get_parameter::<String>("message");
  assert!(message_param.is_ok());
  assert!(message_param.unwrap().contains("main.rs"));
}

#[test]
pub fn test_process_with_requirements() {
  use mcai_worker_sdk::job::Job;
  use mcai_worker_sdk::ParametersContainer;

  let message = r#"{
    "job_id": 123,
    "parameters": [
      {
        "id": "command_template",
        "type": "string",
        "value": "ls {option} {path}"
      },
      {
        "id": "option",
        "type": "string",
        "value": "-lh"
      },
      {
        "id": "path",
        "type": "string",
        "value": "."
      },
      {
        "id": "exec_dir",
        "type": "string",
        "value": "./src"
      },
      {
        "id": "requirements",
        "type": "requirements",
        "value": {
          "paths": [
            "./src"
          ]
        }
      }
    ]
  }"#;

  let job = Job::new(message).unwrap();
  let job_result = JobResult::new(job.job_id);
  let parameters: CommandLineWorkerParameters = job.get_parameters().unwrap();
  let result = process(None, parameters, job_result);

  assert!(result.is_ok());
  let job_result = result.unwrap();
  assert_eq!(123, job_result.get_job_id());
  assert_eq!(&JobStatus::Completed, job_result.get_status());
  let message_param = job_result.get_parameter::<String>("message");
  assert!(message_param.is_ok());
  assert!(message_param.unwrap().contains("main.rs"));
}

#[test]
pub fn test_process_with_error() {
  use mcai_worker_sdk::job::Job;

  let message = r#"{
    "job_id": 123,
    "parameters": [
      {
        "id": "command_template",
        "type": "string",
        "value": "ls {option} {path}"
      },
      {
        "id": "option",
        "type": "string",
        "value": "-lh"
      },
      {
        "id": "path",
        "type": "string",
        "value": "qmslkjggsdlvnqrdgwdnvqrgn"
      },
      {
        "id": "exec_dir",
        "type": "string",
        "value": "./src"
      }
    ]
  }"#;

  let job = Job::new(message).unwrap();
  let job_result = JobResult::new(job.job_id);
  let parameters: CommandLineWorkerParameters = job.get_parameters().unwrap();
  let result = process(None, parameters, job_result);

  assert!(result.is_err());
  let _error = result.unwrap_err();
}
